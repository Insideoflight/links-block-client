<?php
/**
 * Файл содержит все необходимые функции для вывода блока перелинковки на вашем сайте.
 *
 * PHP версия 5
 *
 */

// подключаем config.php с настройками
include_once(dirname(__FILE__)."/config.php");

/**
 * Функция отрисовывает блок перелинковки.
 *
 * Принцип работы:
 * 1. скрипт подключается по HTTP к нашему серверу
 * 2. передавая уникальный ключ и URL текущей страницы
 * 3. в ответ получает массив ссылок в формате json
 * 4. возвращает ссылки в виде готового HTML (ul > li > a)
 * пс.так же скрипт позволяет нашему серверу чистить директорию cache на вашем сервере, в случае если мы внесем изменения в базу ссылок
 *
 * @param string $position название позиции блока, должно быть уникальным для каждого блока на странице. может содержать только след. символы: 0-9a-zA-Z_-
 * @param string $url необязательный параметр, для передачи текущего URL страницы (может пригодится в случае если $_SERVER['REQUEST_URI'] по какой-то причине содержит неверный URL)
 *
 * @return возвращает готовый HTML код блока перелинковки. FALSE в случаях: директрия с кешем (cache) закрыта для записи, путь к директории с кешем указан неверно, имя позиции блока содержит запрещенные символы
 */
function links_block_draw($url=NULL)
{
		if (http_response_code() !== 200) return false;
		if(!isset($GLOBALS['links_block_link_num'])){
			$GLOBALS['links_block_link_num']=1;
		}

		if(is_null($url))$url=$_SERVER['REQUEST_URI'];

		$cache_file_name=md5($url).".cache";
		$cache_dir_path=rtrim(LINKS_BLOCK_CACHE_PATH,"/")."/";

		// обрабатываем команды главного сервера
		if($_POST['links_block_manager_sm']
			&& trim($_POST['key'])==LINKS_BLOCK_KEY){
			$name=trim($_POST['name']);

			switch(trim($_POST['type']))
			{
				case'all':
					 foreach(glob($cache_dir_path."*.cache") AS $file)
					 {
						if(file_exists($file))unlink($file);
					 }
					 die("LINKS_BLOCK_STATUS_OK");
				break;
				case'link':
					$name=preg_replace("#https?://[^/]+/?#","/",$name);
					if(substr($name,0,1)!="/")$name="/".$name;

					foreach(glob($cache_dir_path."*.".md5($name).".cache") AS $file)
					{
						if(file_exists($file))unlink($file);
					}
					die("LINKS_BLOCK_STATUS_OK");
				break;
				case 'block':
					foreach(glob($cache_dir_path.$name.".*.cache") AS $file)
					{
						if(file_exists($file))unlink($file);
					}
					die("LINKS_BLOCK_STATUS_OK");
				break;
				case 'info':
					if(is_writeable($cache_dir_path)) die('--OK--');
					else die('--NO--');
				break;
			}
		}

		if(!is_writeable($cache_dir_path))return false;

		$cache_file_path=$cache_dir_path.$cache_file_name;

		if(!file_exists($cache_file_path)
			|| LINKS_BLOCK_DISABLE_CACHE===true){
			// получаем блок с ссылками из нашего сервера
			$get_block_data_url=LINKS_BLOCK_DATA_URL."&data=".base64_encode(json_encode(array(
				"url"=>$url
			)));
			if(function_exists("curl_init")){
				$ch=curl_init($get_block_data_url);
				curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
				curl_setopt($ch,CURLOPT_TIMEOUT,5);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
				curl_setopt($ch,CURLOPT_HEADER,false);
				$out=curl_exec($ch);
				curl_close($ch);
			}else{
				$ctx=stream_context_create(array(
					"http"=>array(
						"timeout"=>1
					)
				));
				$out=file_get_contents($get_block_data_url,false,$ctx);
			}

			if(empty($out)
				|| $out===false){
				return false;
			}


			$json_out=json_decode($out, true);
			$success = 0;
			foreach($json_out['links'] as $d){
				if (!is_null($d['url']) && !is_null($d['anchor'])){
					$success+=1;
				}
			}
			if ($success == sizeof($json_out['links']))
				file_put_contents($cache_file_path,$out);
		}else{
			$json_out=json_decode(file_get_contents($cache_file_path), true);
		}
		if(is_null($json_out)
			&& $_GET[LINKS_BLOCK_KEY.'_DEBUG']){
			// ошибка парсинга json
			print $out;
			return false;
		}

		if($json_out['debug']===true){
			print "<hr /><strong>RESPONSE DATA:</strong><hr />";
			print nl2br(str_replace(" ","&nbsp;",print_r($json_out,true)));
		}

		$out="";

		$out.='<ul class="lnk-block lnk-block-manager">';
		if (isset($json_out['links']) && is_array($json_out['links'])){
    		foreach($json_out['links'] AS $r)
    		{
                if (is_null($r['url']) || is_null($r['anchor']))
        			continue;
        		if(!preg_match("#^https?://#is",$r['url'])){
        			$r['url']="http://".$r['url'];
        		}
        		$out.='<li><a href="'.$r['url'].'">'.$r['anchor'].'</a>';
        		if (!is_null($r['content']))
        			$out .= '<p>'.$r['content'].'</p>';
        		$out .= '</li>';
    		}
		}
		$out.='</ul>';
		return $out;
}
